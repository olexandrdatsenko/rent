from flask import render_template, request, redirect, url_for
from flask_app import app

from flask_app.models import Organization, Building, \
    NaturalPerson, Rentdocument, LegalPerson, MainDocument, db


@app.route('/')
@app.route('/index')
def index():
    organization = Organization.query.all()
    building = Building.query.all()
    person = NaturalPerson.query.all()
    main_document = MainDocument.query.all()
    return render_template('index.html',
                           title='Home',
                           organizations=organization,
                           buildings=building,
                           persons=person,
                           main_documents=main_document)


@app.route('/result/<name>')
def view_result_name(name):
    org = Organization.query.filter_by(name=name).first_or_404()

    # query = Rentdocument.query\
    #     .join(Building) \
    #     .join(MainDocument) \
    #     .join(NaturalPerson) \
    #     .filter(Building.id_rentdocument == Rentdocument.id,
    #             Rentdocument.id == MainDocument.id_rentdocument,
    #             Rentdocument.id == NaturalPerson.id_rentdocument) \
    #     .order_by(Rentdocument.start_date_rent).all()
    buildings = Building.query.filter_by(id_rentdocument=6).first()
    persons = NaturalPerson.query.filter_by(id_rentdocument=6).first()
    main_documents = MainDocument.query.filter_by(id_rentdocument=6).first()
    rent_document = Rentdocument.query.all()
    return render_template('result.html',
                           title='Result',
                           # query=query,
                           org=org,
                           building=buildings,
                           person=persons,
                           main_document=main_documents,
                           rent_document=rent_document)


@app.route('/result')
def view_result():
    organization = Organization.query.all()
    building = Building.query.all()
    person = NaturalPerson.query.all()
    main_document = MainDocument.query.all()
    return render_template('result.html',
                           title='Result',
                           organization=organization,
                           building=building,
                           person=person,
                           main_document=main_document)


@app.route('/add_organization')
def add_organization():
    return render_template('add_organization.html')


@app.route('/add_person')
def add_person():
    return render_template('add_naturalpers.html')


@app.route('/add_main_document')
def add_main_document():
    return render_template('add_main_document.html')


@app.route('/add_building')
def add_building():
    return render_template('add_building.html')


@app.route('/save_organization', methods=['POST'])
def save_organization():
    organization = Organization(request.form['code'], request.form['name'],
                                request.form['short_name'], request.form['address'],
                                request.form['phone'], request.form['email'])
    db.session.add(organization)
    db.session.commit()
    return redirect(url_for('index'))


@app.route('/save_person', methods=['POST'])
def save_person():
    person = NaturalPerson(request.form['full_name'], request.form['passport_data'],
                            request.form['identity_num_payer'], request.form['address'],
                            request.form['id_rentdocument'])
    db.session.add(person)
    db.session.commit()
    return redirect(url_for('index'))


@app.route('/save_building', methods=['POST'])
def save_building():
    building = Building(request.form['address'], request.form['area_room'],
                        request.form['area_basement'], request.form['rate_basement'],
                        request.form['rate_deb_room'], request.form['id_organization'],
                        request.form['id_rentdocument'])
    db.session.add(building)
    db.session.commit()
    return redirect(url_for('index'))


@app.route('/save_main_document', methods=['Post'])
def save_main_document():
    main_document = MainDocument(request.form['number'],request.form['start_date'],
                                 request.form['base_amaunt'], request.form['id_rentdocument'])
    db.session.add(main_document)
    db.session.commit()
    return redirect(url_for('index'))


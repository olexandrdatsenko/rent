from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from flask_app import app

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://postgres:root@localhost/ucheb'

db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)


# My classes
class Organization(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.Integer, unique=True)
    name = db.Column(db.String(80), unique=True)
    short_name = db.Column(db.String(20))
    address = db.Column(db.String(80))
    phone = db.Column(db.String(15), unique=True)
    email = db.Column(db.String(80), unique=True)
    buldings = db.relationship('Building', backref='oraganization', lazy=True)

    # initial data
    def __init__(self, code, name, short_name, address, phone, email):
        self.code = code
        self.name = name
        self.short_name = short_name
        self.address = address
        self.phone = phone
        self.email = email

    def __repr__(self):
        return '<Organization {}, {}, {}, {}, {}, {}>'.format(self.code, self.name, self.short_name,
                                                              self.address, self.phone, self.email)


class Building(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(80), unique=True)
    area_room = db.Column(db.Float())
    area_basement = db.Column(db.Float())
    rate_basement = db.Column(db.Integer())
    rate_deb_room = db.Column(db.Integer())
    id_organization = db.Column(db.Integer, db.ForeignKey('organization.id'),
                                nullable=False)
    id_rentdocument = db.Column(db.Integer, db.ForeignKey('rentdocument.id'),
                                nullable=False)

    def __init__(self, address, area_room, area_basement,
                 rate_basement, rate_deb_room, id_organization, id_rentdocument):
        self.address = address
        self.area_room = area_room
        self.area_basement = area_basement
        self.rate_basement = rate_basement
        self.rate_deb_room = rate_deb_room
        self.id_organization = id_organization
        self.id_rentdocument = id_rentdocument


    def __repr__(self):
        return '<Building {}, {}, {}, {}, {}, {}>'.format(self.address, self.area_room,
                                                          self.area_basement,
                                                          self.rate_basement, self.rate_deb_room,
                                                          self.id_organization, self.id_rentdocument)


class Rentdocument(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    start_date_rent = db.Column(db.Date)
    contract_date = db.Column(db.Date)
    naturalpersons = db.relationship('NaturalPerson', backref='rentdocument', lazy=True)
    legalpersons = db.relationship('LegalPerson', backref='rentdocument', lazy=True)
    maindocuments = db.relationship('MainDocument', backref='rentdocument', lazy=True)
    buildings = db.relationship('Building', backref='rentdocument', lazy=True)

    def __init__(self, start_date_rent, contract_date):
        self.start_date_rent = start_date_rent
        self.contract_date = contract_date

    def __repr__(self):
        return '<RentDocument {}, {}>'.format(self.start_date_rent, self.contract_date)


class NaturalPerson(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    full_name = db.Column(db.String(80))
    passport_data = db.Column(db.String(80), unique=True)
    identity_num_payer = db.Column(db.Integer, unique=True)
    address = db.Column(db.String(80), unique=True)
    id_rentdocument = db.Column(db.Integer, db.ForeignKey('rentdocument.id'),
                                nullable=False)

    def __init__(self, full_name, passport_data, identity_num_payer, address, id_rentdocument):
        self.full_name = full_name
        self.passport_data = passport_data
        self.identity_num_payer = identity_num_payer
        self.address = address
        self.id_rentdocument = id_rentdocument

    def __repr__(self):
        return '<NaturalPerson {}, {}, {}, {}, {}>'.format(self.full_name, self.passport_data,
                                                              self.identity_num_payer,
                                                              self.address, self.id_rentdocument)


class LegalPerson(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    address = db.Column(db.String(80), unique=True)
    identity_num_payer = db.Column(db.Integer, unique=True)
    license_num = db.Column(db.Integer)
    licence_date = db.Column(db.Date)
    id_rentdocument = db.Column(db.Integer, db.ForeignKey("rentdocument.id"),
                                nullable=False)

    def __init__(self, name, address, identity_num_payer,
                 licence_num, licence_date, id_rentdocument):
        self.name = name
        self.address = address
        self.identity_num_payer = identity_num_payer
        self.license_num = licence_num
        self.licence_date = licence_date
        self.id_rentdocument = id_rentdocument

    def __repr__(self):
        return '<LegalPerson {}, {}, {}, {}, {}, {}>'.format(self.name, self.address, self.identity_num_payer,
                                                              self.license_num, self.licence_date,
                                                              self.id_rentdocument)


class MainDocument(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    number = db.Column(db.Integer)
    start_date = db.Column(db.Date)
    base_amaunt = db.Column(db.Float)
    id_rentdocument = db.Column(db.Integer, db.ForeignKey('rentdocument.id'),
                                nullable=False)

    def __init__(self, number, start_date, base_amaunt, id_rentdocument):
        self.number = number
        self.start_date = start_date
        self.base_amaunt = base_amaunt
        self.id_rentdocument = id_rentdocument

    def __repr__(self):
        return '<MainDocument {}, {}, {}, {}>'.format(self.number, self.start_date,
                                                      self.base_amaunt, self.id_rentdocument)
if __name__ == "__main__":
    manager.run()